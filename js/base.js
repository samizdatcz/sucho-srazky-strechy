var vodaCena = { //data ČSÚ 2015: https://www.czso.cz/csu/xj/vodovody-a-kanalizace-v-roce-2016-vodne-a-stocne-dale-roste
	'Hlavní město Praha': {'vodne': 40.1040638770668, 'stocne': 32.2505738332058},
	'Středočeský kraj': {'vodne': 38.9753843141095, 'stocne': 31.3300163702246},
	'Jihočeský kraj': {'vodne': 35.4493548636328, 'stocne': 28.5534750489974},
	'Plzeňský kraj': {'vodne': 37.5333739837398, 'stocne': 26.674520069808},
	'Karlovarský kraj': {'vodne': 36.6361802754208, 'stocne': 33.788587279572},
	'Ústecký kraj': {'vodne': 43.0187594472036, 'stocne': 41.3771345704223},
	'Liberecký kraj': {'vodne': 42.1789706755954, 'stocne': 41.9244000979432},
	'Královéhradecký kraj': {'vodne': 34.2019504617243, 'stocne': 33.0788964381305},
	'Pardubický kraj': {'vodne': 31.8837240218017, 'stocne': 34.6887830229537},
	'Kraj Vysočina': {'vodne': 35.3561637426901, 'stocne': 26.315156662048},
	'Jihomoravský kraj': {'vodne': 33.4709033402198, 'stocne': 33.2424698031928},
	'Olomoucký kraj': {'vodne': 32.0716964743969, 'stocne': 30.9696362611908},
	'Zlínský kraj': {'vodne': 35.3737079822809, 'stocne': 29.7917146144994},
	'Moravskoslezský kraj': {'vodne': 33.2595495464183, 'stocne': 30.4913808013837}
};

function niceNum(num) {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
        osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        osm = L.tileLayer(osmUrl, { maxZoom: 19, attribution: osmAttrib }),
        map = new L.Map('map', { center: new L.LatLng(49.7417517, 15.3350758), zoom: 8 }),
        drawnItems = L.featureGroup().addTo(map);

map.scrollWheelZoom.disable();

L.control.layers({
    'Klasická mapa': osm.addTo(map),
    'Letecká mapa': L.tileLayer('http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}', {
        attribution: 'google'
    })
}, {}, { position: 'topleft', collapsed: false }).addTo(map);

var srazky_kraje = L.geoJson(kraje, {
	style: function(feature) {
		return {
			'kraj': feature.properties.kraj,
			'srazky_mm': feature.properties.srazky_mm
		}
	}
});

var editing = false;
map.on('click', function(e) {
  if(!editing){ // do not start multiple "edit sessions"
  	drawnItems.clearLayers();
    editing = true;
    var polyEdit = new L.Draw.Polygon(map);
    polyEdit.enable();
    polyEdit.addVertex(e.latlng);
  }
});

document.onkeydown = function(e) {
	if (e.code == 'Escape') {
		editing = false;
	};
};

function makeBlabol(m3vody, kraj, roofArea) {

	var blabol = '<b>Za vodu byste mohli ušetřit ' 
		+ niceNum(Math.round(((m3vody * vodaCena[kraj.kraj].vodne * 1.15) + (m3vody * 0.9 * vodaCena[kraj.kraj].stocne * 1.15))))  
		+ ' Kč</b></br>Ročně sem (na plochu ' 
		+ Math.round(roofArea * 10) / 10
		+ ' m2) naprší asi ' 
		+ niceNum(m3vody) 
		+ ' kubíků vody. Průměrná cena vodného pro ' 
		+ kraj.kraj 
		+ ' je ' 
		+ Math.round(vodaCena[kraj.kraj].vodne * 1.15 * 100) / 100 
		+ ' Kč/m3. Kdyby se vám dešťovou vodu z této střechy podařilo zachytit a zužitkovat, mohli byste na vodném ušetřit ' 
		+ niceNum(Math.round(m3vody * vodaCena[kraj.kraj].vodne * 1.15)) 
		+ ' Kč a na stočném ' 
		+ niceNum(Math.round(m3vody * 0.9 * vodaCena[kraj.kraj].stocne * 1.15)) 
		+ ' včetně DPH. Od stočného z dešťové vody jsou domy užívané k bydlení osvobozeny.</br>'

	if ($('.stocneswitch').is(":checked") == false) {
		// http://voda.tzb-info.cz/destova-voda/2713-platba-za-srazkove-vody
		var blabol = '<b>Za vodu byste mohli ušetřit ' 
		+ niceNum(Math.round(((m3vody * vodaCena[kraj.kraj].vodne * 1.15) - (m3vody * 0.9 * vodaCena[kraj.kraj].stocne * 1.15))))  
		+ ' Kč</b></br>Ročně sem (na plochu ' 
		+ Math.round(roofArea * 10) / 10
		+ ' m2) naprší asi ' 
		+ niceNum(m3vody) 
		+ ' kubíků vody. Průměrná cena vodného pro ' 
		+ kraj.kraj 
		+ ' je ' 
		+ Math.round(vodaCena[kraj.kraj].vodne * 1.15 * 100) / 100 
		+ ' Kč/m3. Kdyby se vám dešťovou vodu z této střechy podařilo zachytit a zužitkovat, mohli byste na vodném ušetřit ' 
		+ niceNum(Math.round(m3vody * vodaCena[kraj.kraj].vodne * 1.15)) 
		+ ' Kč, nicméně byste zaplatili na stočném ' 
		+ niceNum(Math.round(m3vody * 0.9 * vodaCena[kraj.kraj].stocne * 1.15)) 
		+ ' včetně DPH.</br>'
	};

	return blabol;
};

map.on(L.Draw.Event.CREATED, function (event) {
    var layer = event.layer;
    drawnItems.addLayer(layer);
    var trg = layer.getLatLngs()[0];
    var kraj = leafletPip.pointInLayer([trg.lng, trg.lat], srazky_kraje)[0].feature.properties;

    var roofArea = L.GeometryUtil.geodesicArea(layer.getLatLngs());
    var m3vody = Math.round((roofArea * kraj.srazky_mm) / 1000);
    $('.main_info').html(makeBlabol(m3vody, kraj, roofArea));
	editing = false;

	$('.stocneswitch').change(function() {
		$('.main_info').html(makeBlabol(m3vody, kraj, roofArea));
	});
});

var form = document.getElementById("frm-geocode");
var geocoder = null;
var geocodeMarker = null;
form.onsubmit = function(event) {
	event.preventDefault();
	var text = document.getElementById("inp-geocode").value;
	if (text == '') {
		var center = new L.LatLng(49.7417517, 15.3350758);
		map.setView(center, 8);
	} else {
		$.get( "https://api.mapy.cz/geocode?query=" + text, function(data) {
			if (typeof $(data).find('item').attr('x') == 'undefined') {
				alert("Bohužel, danou adresu nebylo možné najít");
				return;
			}
			var latlng = new L.LatLng($(data).find('item').attr('y'), $(data).find('item').attr('x'));
			map.setView(latlng, 15);
		}, 'xml');
	}	
};